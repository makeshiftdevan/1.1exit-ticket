

//Array of objects
const quiz = [
{
	q: "A man on a motorcycle is initially at point A.  At time t = 0, the man travels along the path shown to point B.  The amount of time required to complete each segment of the path is also indicated in the diagram.  [Note:  The squares in the grid are 10 meters long.]  What distance does the man travel in going from point A to point B?",
	imgurl: 'https://photos.app.goo.gl/u4PttNTHfeLURN7N7', 
	options: ['15 meters','30 meters','60 meters','90 meters', '120 meters'],
	answer: 4,
	topic: "Distance vs. Displacement from a graph"
},
  
{
	q: "A man on a motorcycle is initially at point A.  At time t = 0, the man travels along the path shown to point B.  The amount of time required to complete each segment of the path is also indicated in the diagram.  [Note:  The squares in the grid are 10 meters long.] What is the magnitude of the man's displacement once he reaches point B?",
	imgfile: '/1.1.1.gif',
	options:['15 meters','30 meters','60 meters','90 meters', '120 meters'],
	answer:2,
	topic: "Distance vs. Displacement from a graph"
},
{
	q: "A man on a motorcycle is initially at point A.  At time t = 0, the man travels along the path shown to point B.  The amount of time required to complete each segment of the path is also indicated in the diagram.  [Note:  The squares in the grid are 10 meters long.] What is the man's average speed as he travels from point A to point B?",
	imgfile: '/1.1.1.gif',
	options:['2 m/s','3 m/s','6 m/s','7 m/s', '18 m/s'],
	answer:2,
	topic: "Average Speed vs Velocity"
},
{
	q: "A man on a motorcycle is initially at point A.  At time t = 0, the man travels along the path shown to point B.  The amount of time required to complete each segment of the path is also indicated in the diagram.  [Note:  The squares in the grid are 10 meters long.] What is the magnitude of the man's average velocity for this travel?",
	imgfile: '/1.1.1.gif',
	options:['2 m/s','3 m/s','6 m/s','7 m/s', '18 m/s'],
	answer:1,
	topic: "Average Speed vs Velocity"
},
{
	q: "A man on a motorcycle is initially at point A.  At time t = 0, the man travels along the path shown to point B.  The amount of time required to complete each segment of the path is also indicated in the diagram.  [Note:  The squares in the grid are 10 meters long.] What is the man's instantaneous speed at time t = 19 seconds?",
	imgfile: '1.1.1.gif',
	options:['3 m/s','5 m/s','6 m/s','8 m/s', '10 m/s'],
	answer:4,
	topic: "Instantaneous Speed"
}

]