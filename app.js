

const questionNumber = document.querySelector(".question-number");
const questionText = document.querySelector(".question-text");
const optionContainer = document.querySelector(".option-container");
const image = document.createElement("img");
const homeBox = document.querySelector(".home-box");
const firstBox = document.querySelector(".first-box");
const errorTen = "notValid";
const quizBox = document.querySelector(".quiz-box");
const resultBox = document.querySelector(".result-box");
const SPREADSHEET_ID = '1QyB3WT1LrfJDNjiaVvLQYkGlu_GB8fT3yXDdjXGsoFc'; 
const CLIENT_ID = '15494048086-sg57jbj9cjdlmuiekvt7sd1cdh2p6qb3.apps.googleusercontent.com';
const SCOPE = 'https://www.googleapis.com/auth/spreadsheets';
const API_KEY = 'AIzaSyCgv5WIgpm4DaWQFgSuVd2wAcLxRparQ50';


image.src  = "../1.1.1.gif"


let questionCounter = 0;
let currentQuestion;
let availableQuestions = [];
let availableOptions = [];
let correctAnswers = 0;
let attempt = 0;
let topicList = [];





//push the questions into availableQuestions Array
function setAvailableQuestions(){
	const totalQuestion = quiz.length;
	for(let i=0; i<totalQuestion;
		i++){
		availableQuestions.push(quiz[i])
	}
}

function quizEnd(){
	var fileNotFound = document.querySelector("input");
	document.getElementById("validation").addEventListener('submit', validate) 
	function validate(e){
		e.preventDefault();
		let valid = true;
	if (fileNotFound.value === "notValid") {
		enterInfo();
		} else {	 
			alert("I'm telling.  This attempt and IP address have been logged into the data base.");
}
}
	}

function enterInfo(){
	homeBox.classList.remove("hide");
	firstBox.classList.add("hide");
}


function startQuiz(){
	alert('Remember to only select your "Final Answer".  Good Luck!', response => startQuiz());
	//hide the homeBox
	homeBox.classList.add("hide");
	//show resultBox
	quizBox.classList.remove("hide");

}

//set question number, question, and options
function getNewQuestion(){
	//set question number
	questionNumber.innerHTML = "Question " + (questionCounter + 1) + " of " + quiz.length;

	//set question text
	//get random question
	const questionIndex = availableQuestions[Math.floor(Math.random() * availableQuestions.length)]
	currentQuestion = questionIndex;
	questionText.innerHTML = currentQuestion.q;
	//add image to question
	document.querySelector('.question-text').appendChild(image);
	//get the position of 'questionIndex' from availableQuestion index
	const index1 = availableQuestions.indexOf(questionIndex);
	//remove the questionIndex so questions do not repeat
	availableQuestions.splice(index1, 1);
	

	//set options
	// get the length of options
	const optionLen = currentQuestion.options.length
	//push options into availableOptions Array
	for(let i=0; i<optionLen; i++){
		availableOptions.push(i)
	}
	optionContainer.innerHTML = '';
	let animationDelay = 0.2;
	//create options in html
	for(let i=0; i<optionLen; i++){
		// random option
		const optonIndex = availableOptions[Math.floor(Math.random() * availableOptions.length)];
		// remove the 'optionIndex' from availableOptions so there is not a repeat option
		const index2 = availableOptions.indexOf(optonIndex);
		//remove the 'optonIndex' from the availableOptions so there are no repeat options
		availableOptions.splice(index2, 1);
		const option = document.createElement("div");
		option.innerHTML = currentQuestion.options[optonIndex];
		option.id = optonIndex;
		option.style.animationDelay = animationDelay + 's';
		animationDelay = animationDelay + 0.2;
		option.className = "option";
		optionContainer.appendChild(option)
		option.setAttribute("onclick", "getResult(this)");
	}
	questionCounter++
}	

//get the result of the current attempt question
function getResult(element){
	const id = parseInt(element.id);
	//get the answer by comparing the id of clicked option
	if(id === currentQuestion.answer){
		//set the color to the correct option (green?)
		element.classList.add("correct");
		correctAnswers++;
	}
	else{
		//set the color to the wrong option (red?)
		element.classList.add("wrong");
		topicList.push(currentQuestion.topic);

	}
	attempt++;
	unclickableOptions();
}


// make all the options unclickable after a choice has been selected
function unclickableOptions(){
	const optionLen = optionContainer.children.length;
	for (let i=0; i<optionLen; i++){
		optionContainer.children[i].classList.add("already-answered");
	}

}

function next(){
	if(questionCounter === quiz.length){
		console.log("quiz over")
		alert('Submit and view results?')
		quizOver();
	}
	else{
		getNewQuestion();
	}	
	
}

function quizOver(){
	//hide the quiz box
	quizBox.classList.add("hide");
	//show resultBox
	resultBox.classList.remove("hide");
	quizResult();

}
//get the results
function quizResult(){

	var percentage = (correctAnswers / quiz.length) * 100;
	resultBox.querySelector(".total-question").innerHTML = quiz.length;
	resultBox.querySelector(".total-attempt").innerHTML = attempt;
	resultBox.querySelector(".total-correct").innerHTML = correctAnswers;
	resultBox.querySelector(".total-wrong").innerHTML = attempt - correctAnswers;
	resultBox.querySelector(".percentage").innerHTML = percentage.toFixed() + "%";
	resultBox.querySelector(".total-score").innerHTML = correctAnswers + "/" + quiz.length;
	resultBox.querySelector(".topic-list").innerHTML = topicList;
	getResults();

}

//post results to Google Sheet
function getResults(){
	const scriptURL = "https://script.google.com/macros/s/AKfycbwa5S06ds7-alPvPjSsQjLQj_6vlcOJqshmkiESar2hp6e--tT0PdcYAxPHRWoxR7g0/exec"
	var percentage = (correctAnswers / quiz.length) * 100;
	var form = document.forms['google-sheet']
	var fetchBody = new FormData(form);
	fetchBody.append('percentage',percentage);
	for (var i = 0; i <topicList.length; i++) {
  		fetchBody.append('topicList[]', topicList[i]);
		}
	postResults();

	function postResults(){;
	fetch(scriptURL, { method: 'POST', body: fetchBody});	
	}
	}	


window.onload = function(){
	quizBox.classList.add("hide");
	// first set all questions available in availableQuestions Array
	setAvailableQuestions();
	// Next, get a new question
	getNewQuestion();
}
